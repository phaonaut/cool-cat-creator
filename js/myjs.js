
$(document).ready(function(){
    
    $("#clear").click(function(){
        $(".ds").hide();
    
    });
    
    $("#aristocat").click(function(){
        $(".ds").hide();
        $(".tophat").show();
        $(".mustache").show();
        $(".monocle").show();
    });
    
    
    $("#cboy").click(function(){
        $(".ds").hide();
        $(".kangol").show();
        $(".chain").show();
        $(".shades").show();
    });    
        
        
    
    $("#tophat").click(function(){
        $(".h").hide();
        $(".tophat").toggle();
    });
    $("#pirate").click(function(){
        $(".h").hide();
        $(".pirate").toggle();
    });
    $("#kangol").click(function(){
        $(".h").hide();
        $(".kangol").toggle();
    });
    $("#cowboy").click(function(){
        $(".h").hide();
        $(".cowboy").toggle();
    });
    
    
    
    $("#monocle").click(function(){
        $(".e").hide();        
        $(".monocle").toggle();
    });
    $("#shades").click(function(){
        $(".e").hide();
        $(".shades").toggle();
    });
    $("#glasses").click(function(){
        $(".e").hide();
        $(".glasses").toggle();
    });
    $("#eyepatch").click(function(){
        $(".e").hide();
        $(".eyepatch").toggle();
    });
    
    
    
    $("#mustache").click(function(){
        $(".m").hide();
        $(".mustache").toggle();
    });
    $("#cig").click(function(){
        $(".m").hide();
        $(".cig").toggle();
    });
    $("#kerchief").click(function(){
        $(".m").hide();
        $(".kerchief").toggle();
    });
    $("#chain").click(function(){
        $(".m").hide();
        $(".chain").toggle();
    });

    
    
    $("#earring").click(function(){
        $(".er").hide();
        $(".earring").toggle();
    });
    $("#cross").click(function(){
        $(".er").hide();
        $(".cross").toggle();
    });
    $("#dblloop").click(function(){
        $(".er").hide();
        $(".dblloop").toggle();
    });
    
    
    
});

